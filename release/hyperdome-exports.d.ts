import { EventEmitter } from 'events';

declare class ExtensionBase {
    readonly modules: Map<string, any>;
    preInstall: () => void;
    install: () => void;
    postInstall: () => void;
}
declare class Extension extends ExtensionBase {
    constructor();
}
declare class ExtensionGroup extends ExtensionBase {
    constructor();
}

interface AppConfigs {
    root: string;
    appDataPath: string;
    modulesDir: string;
    extensionsDir: string;
    [key: string]: any;
}

declare class AITModuleBase extends EventEmitter {
    constructor();
    install: () => Promise<void> | void;
    uninstall: () => void;
    update: () => void;
    login: () => void;
    logout: () => void;
}
declare abstract class AITModule extends AITModuleBase {
    static newInstance: (configs: AppConfigs) => AITModule;
}

export { AITModule, AppConfigs, Extension, ExtensionGroup };
