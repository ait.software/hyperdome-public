"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __name = (target, value) => __defProp(target, "name", { value, configurable: true });
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);
var __publicField = (obj, key, value) => {
  __defNormalProp(obj, typeof key !== "symbol" ? key + "" : key, value);
  return value;
};

// src/main/hyperdome-exports.ts
var hyperdome_exports_exports = {};
__export(hyperdome_exports_exports, {
  AITModule: () => AITModule,
  Extension: () => Extension,
  ExtensionGroup: () => ExtensionGroup
});
module.exports = __toCommonJS(hyperdome_exports_exports);

// src/main/ait-extension.ts
var ExtensionBase = /* @__PURE__ */ __name(class ExtensionBase2 {
  modules = /* @__PURE__ */ new Map();
  preInstall = () => {
  };
  install = () => {
  };
  postInstall = () => {
  };
}, "ExtensionBase");
var Extension = class extends ExtensionBase {
  constructor() {
    super();
    this.modules.set("database", {});
  }
};
__name(Extension, "Extension");
var ExtensionGroup = class extends ExtensionBase {
  constructor() {
    super();
  }
};
__name(ExtensionGroup, "ExtensionGroup");

// src/main/ait-module.ts
var import_events = require("events");
var AITModuleBase = /* @__PURE__ */ __name(class AITModuleBase2 extends import_events.EventEmitter {
  constructor() {
    super();
    if (!this.constructor.newInstance) {
      throw new Error('The "static newInstance: () => AITModule" method is not implemented!');
    }
  }
  install = () => {
  };
  uninstall = () => {
  };
  update = () => {
  };
  login = () => {
  };
  logout = () => {
  };
}, "AITModuleBase");
var AITModule = class extends AITModuleBase {
};
__name(AITModule, "AITModule");
__publicField(AITModule, "newInstance");
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  AITModule,
  Extension,
  ExtensionGroup
});
